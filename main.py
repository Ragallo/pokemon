from constants import (
    AttackCategory, 
    Commands,
    Nature,
    Stats,
    Types,
)
from models import (
    Attack, 
    Battle, 
    Command, 
    Pokemon,  
    Turn
)

# First, define pokemons with its stats

pokemon1 = Pokemon(
    name="Pikachu", 
    level=100, 
    type1=Types.ELECTRIC.value["index"], 
    type2=None,
)
pokemon1.current_hp = 45

pokemon2 = Pokemon(
    name="Gyaradps", 
    level=100, 
    type1=Types.WATER.value["index"], 
    type2=Types.FLYING.value["index"],
)
pokemon2.current_hp = 39

# Stats
pokemon1.base_stats = {
    Stats.HP.value:39,
    Stats.ATTACK.value:52,
    Stats.DEFENSE.value:43,
    Stats.SPATTACK.value:80,
    Stats.SPDEFENSE.value:65,
    Stats.SPEED.value:65,
}
pokemon1.iv = {
    Stats.HP.value:21,
    Stats.ATTACK.value:21,
    Stats.DEFENSE.value:21,
    Stats.SPATTACK.value:21,
    Stats.SPDEFENSE.value:21,
    Stats.SPEED.value:21,
}
pokemon1.ev = {
    Stats.HP.value:0,
    Stats.ATTACK.value:0,
    Stats.DEFENSE.value:0,
    Stats.SPATTACK.value:0,
    Stats.SPDEFENSE.value:0,
    Stats.SPEED.value:0,
}
pokemon1.nature = Nature.ADAMANT.value
pokemon1.compute_stats()
print(pokemon1.stats)

pokemon2.base_stats = {
    Stats.HP.value:39,
    Stats.ATTACK.value:52,
    Stats.DEFENSE.value:43,
    Stats.SPATTACK.value:80,
    Stats.SPDEFENSE.value:65,
    Stats.SPEED.value:65,
}
pokemon2.iv = {
    Stats.HP.value:0,
    Stats.ATTACK.value:0,
    Stats.DEFENSE.value:0,
    Stats.SPATTACK.value:0,
    Stats.SPDEFENSE.value:0,
    Stats.SPEED.value:0,
}
pokemon2.ev = {
    Stats.HP.value:21,
    Stats.ATTACK.value:21,
    Stats.DEFENSE.value:21,
    Stats.SPATTACK.value:21,
    Stats.SPDEFENSE.value:21,
    Stats.SPEED.value:21,
}
pokemon2.nature = Nature.HARDY.value
pokemon2.compute_stats()
# Attacks
pokemon1.attacks = [
    Attack(
        name="Impac Trueno",
        t=Types.ELECTRIC.name,
        category=AttackCategory.SPECIAL.value,
        pp=12,
        power=10,
        accuracy=100,
    )
]

pokemon2.attacks = [
    Attack(
        name="scratch",
        t=Types.NORMAL.name,
        category=AttackCategory.PHYSICAL.value,
        pp=10,
        power=10,
        accuracy=100,
    )
]


# Start battle
battle = Battle(pokemon1=pokemon1, pokemon2=pokemon2)

def ask_command(pokemon: Pokemon):
    command = None
    while not command:
        # DO ATTACK -> attack 0
        tmp_command = input(f"What should {pokemon.name} do? ").split()
        if len(tmp_command) == 2:
            try:
                do_attack = tmp_command[0] == Commands.DO_ATTACK.value
                valid_attack_index = 0 <= int(tmp_command[1]) < 4
                if do_attack and valid_attack_index:
                    command = Command({Commands.DO_ATTACK.value:int(tmp_command[1])})
            except Exception:
                pass
    return command

while not battle.is_finished():
    command1 = ask_command(pokemon1)
    command2 = ask_command(pokemon2)

    turn = Turn()
    turn.command1 = command1
    turn.command2 = command2

    if turn.can_start():
        battle.excecute_turn(turn)
        battle.print_current_status()
