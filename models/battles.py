"""Battels models."""

import random
from typing import Dict

from constants import AttackCategory, Commands, Stats, Types
from models.pokemons import Attack, Pokemon


class Command:
    def __init__(self, action: Dict):
        self.action = action


class Turn:
    def __init__(self):
        self.command1 = None
        self.command2 = None
    
    def can_start(self) -> bool:
        return self.command1 is not None and self.command2 is not None


class Battle:
    def __init__(self, pokemon1: Pokemon, pokemon2: Pokemon):
        self.pokemon1 = pokemon1
        self.pokemon2 = pokemon2
        self.actual_turn = 0

    def is_finished(self) -> bool:
        finished = self.pokemon1.current_hp <= 0 or self.pokemon2.current_hp <= 0
        if finished:
            self.print_winner()
        return finished

    def excecute_turn(self, turn: Turn):
        command1 = turn.command1
        command2 = turn.command2
        attack1 = None
        attack2 = None

        if Commands.DO_ATTACK.value in command1.action.keys():
            attack1 = self.pokemon1.attacks[command1.action[Commands.DO_ATTACK.value]]
        
        if Commands.DO_ATTACK.value in command2.action.keys():
            attack2 = self.pokemon2.attacks[command2.action[Commands.DO_ATTACK.value]]

        self.pokemon2.current_hp -= self.compute_damage(attack1, self.pokemon1, self.pokemon2)
        self.pokemon1.current_hp -= self.compute_damage(attack2, self.pokemon2, self.pokemon1)

        print(f"{self.pokemon1.name} makes: {self.compute_damage(attack1, self.pokemon1, self.pokemon2)} dmg")
        print(f"{self.pokemon2.name} makes: {self.compute_damage(attack2, self.pokemon2, self.pokemon1)} dmg")

        self.actual_turn += 1

    def print_winner(self):
        if self.pokemon1.current_hp <= 0 < self.pokemon2.current_hp:
            print(f"{self.pokemon2.name} won in {str(self.actual_turn)} turns!")
        elif self.pokemon2.current_hp <= 0 < self.pokemon1.current_hp:
            print(f"{self.pokemon1.name} won in {str(self.actual_turn)} turns!")
        else:
            print("Double KO!!!")
    
    def print_current_status(self):
        print(f"{self.pokemon1.name} has {self.pokemon1.current_hp}hp left")
        print(f"{self.pokemon2.name} has {self.pokemon2.current_hp}hp left")
        print()

    def compute_damage(self, attack: Attack, pokemon1: Pokemon, pokemon2: Pokemon) -> int:
        aux = ((2 * pokemon1.level) / 5) + 2
        power_factor = aux * attack.power

        if attack.type == AttackCategory.PHYSICAL.value:
            power_factor *= (
                pokemon1.stats[Stats.ATTACK.value] / pokemon2.stats[Stats.DEFENSE.value]
            )

        else:
            power_factor *= (
                pokemon1.stats[Stats.SPATTACK.value] / pokemon2.stats[Stats.SPDEFENSE.value]
            )
        
        damage_with_out_modifier = power_factor / 50 + 2

        return damage_with_out_modifier * self.compute_damage_modifier(attack, pokemon1, pokemon2)

    def compute_damage_modifier(self, attack: Attack, pokemon1: Pokemon, pokemon2: Pokemon) -> int:
        """Dividir esto en funciones"""
        # Compute STAB
        stab = 1
        attack_type = Types._member_map_[attack.type]._value_["index"] # Resolver esto!! ipdb
        if attack_type == pokemon1.type1 or attack_type == pokemon1.type2:
            print("HAS STAB")
            stab = 1.5

        # Compute Type effectivness
        effectivness1 = Types._member_map_[attack.type]._value_["effect"][pokemon2.type1]
        effectivness2 = 1

        if pokemon2.type2 is not None:
            effectivness2 = Types._member_map_[attack.type]._value_["effect"][pokemon2.type2] or 1

        final_effectiveness = effectivness1 * effectivness2 

        if random.random() < 0.2:
            critical = 1.5

        return  stab * final_effectiveness * critical
