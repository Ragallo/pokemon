"""Pokemons models"""

from typing import Optional

from constants import Stats


class Pokemon:
    def __init__(self, name: str, level: int, type1: str, type2: Optional[str]):
        self.name = name
        self.level = level
        self.type1 = type1
        self.type2 = type2
        self.attacks = [] # vector de ataques
        self.stats = dict()
        self.base_stats = dict()
        self.ev = dict()
        self.iv = dict()
        self.current_status = 0
        self.current_hp = 0
        self.nature = None

    def compute_stats(self):
        self.stats = {
            Stats.HP.value:self.compute_hp_stat(),
            Stats.ATTACK.value:self.compute_standar_stat(Stats.ATTACK.value),
            Stats.DEFENSE.value:self.compute_standar_stat(Stats.DEFENSE.value),
            Stats.SPATTACK.value:self.compute_standar_stat(Stats.SPATTACK.value),
            Stats.SPDEFENSE.value:self.compute_standar_stat(Stats.SPDEFENSE.value),
            Stats.SPEED.value:self.compute_standar_stat(Stats.SPEED.value),
        }

    def compute_standar_stat(self, stat: Stats):
        stat_value = (
            2 
            * self.base_stats[stat] 
            + self.iv[stat] 
            + int(self.ev[stat] / 4)
        ) * self.level
        return (int(stat_value / 100) + 5) * self.nature[stat]

    def compute_hp_stat(self):
        stat_value = (
            2 
            * self.base_stats[Stats.HP.value] 
            + self.iv[Stats.HP.value] 
            + int(self.ev[Stats.HP.value] / 4)
        ) * self.level
        return int(stat_value / 100) + self.level + 10

class Attack:
    def __init__(self, name: str, t: str, category: str, pp: int, power: int, accuracy: int):
        self.name = name
        self.type = t
        self.category = category
        self.pp = pp
        self.power = power
        self.accuracy = accuracy
