from enum import Enum


class Stats(Enum):
    HP = "HP"
    ATTACK = "Attack"
    DEFENSE = "Defense"
    SPATTACK = "SpAttack"
    SPDEFENSE = "SpDefense"
    SPEED = "Speed"


class AttackCategory(Enum):
    PHYSICAL = "physical"
    SPECIAL = "special"


class Commands(Enum):
    DO_ATTACK = "attack"
    DO_ATTACK_SELECTION = "selected_attack"


class Nature(Enum):
    HARDY = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }
    LONELY = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1.1,
        Stats.DEFENSE.value:0.9,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }
    BRAVE = {
        Stats.HP.value:1, 
        Stats.ATTACK.value:1.1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:0.9
    }
    ADAMANT = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1.1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:0.9,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }
    NAUGHTY = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1.1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:0.9,
        Stats.SPEED.value:1
    }
    BOLD = {
        Stats.HP.value:1,
        Stats.ATTACK.value:0.9,
        Stats.DEFENSE.value:1.1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }
    DOCILE = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }
    RELAXED = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1.1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:0.9
    }
    IMPISH = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1.1,
        Stats.SPATTACK.value:0.9,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }
    LAX = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1.1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:0.9,
        Stats.SPEED.value:1
    }
    TIMID = {
        Stats.HP.value:1,
        Stats.ATTACK.value:0.9,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1.1
    }
    HASTY = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:0.9,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1.1
    }
    SERIOUS = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }
    JOLLY = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:0.9,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1.1
    }
    NAIVE = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:0.9,
        Stats.SPEED.value:1.1
    }
    MODEST = {
        Stats.HP.value:1,
        Stats.ATTACK.value:0.9,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1.1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }
    MILD = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:0.9,
        Stats.SPATTACK.value:1.1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }
    QUIET = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1.1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:0.9
    }
    BASHFUL = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }
    RASH = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1.1,
        Stats.SPDEFENSE.value:0.9,
        Stats.SPEED.value:1
    }
    CALM = {
        Stats.HP.value:1,
        Stats.ATTACK.value:0.9,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1.1,
        Stats.SPEED.value:1
    }
    GENTLE = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:0.9,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1.1,
        Stats.SPEED.value:1
    }
    SASSY = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1.1,
        Stats.SPEED.value:0.9
    }
    CAREFUL = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:0.9,
        Stats.SPDEFENSE.value:1.1,
        Stats.SPEED.value:1
    }
    QUIRKY = {
        Stats.HP.value:1,
        Stats.ATTACK.value:1,
        Stats.DEFENSE.value:1,
        Stats.SPATTACK.value:1,
        Stats.SPDEFENSE.value:1,
        Stats.SPEED.value:1
    }


class Types(Enum):
    NORMAL = dict(index=0, effect=[1,1,1,1,1,.5,1,0,.5,1,1,1,1,1,1,1,1,1])
    FIGHTING = dict(index=1, effect=[2,1,.5,.5,1,2,.5,0,2,1,1,1,1,.5,2,1,2,.5])
    FLYING = dict(index=2, effect=[1,2,1,1,1,.5,2,1,.5,1,1,2,.5,1,1,1,1,1])
    POISON = dict(index=3, effect=[1,1,1,.5,.5,.5,1,.5,0,1,1,2,1,1,1,1,1,2])
    GROUND = dict(index=4, effect=[1,1,0,2,1,2,.5,1,2,2,1,.5,2,1,1,1,1,1])
    ROCK = dict(index=5, effect=[1,.5,2,1,.5,1,2,1,.5,2,1,1,1,1,2,1,1,1])
    BUG = dict(index=6, effect=[1,.5,.5,.5,1,1,1,.5,.5,.5,1,2,1,2,1,1,2,.5])
    GHOST = dict(index=7, effect=[0,1,1,1,1,1,1,2,1,1,1,1,1,2,1,1,.5,1])
    STEEL = dict(index=8, effect=[1,1,1,1,1,2,1,1,.5,.5,.5,1,.5,1,2,1,1,2])
    FIRE = dict(index=9, effect=[1,1,1,1,1,.5,2,1,2,.5,.5,2,1,1,2,.5,1,1])
    WATER = dict(index=10, effect=[1,1,1,1,2,2,1,1,1,2,.5,.5,1,1,1,.5,1,1])
    GRASS = dict(index=11, effect=[1,1,.5,.5,2,2,.5,1,.5,.5,2,.5,1,1,1,.5,1,1])
    ELECTRIC = dict(index=12, effect=[1,1,2,1,0,1,1,1,1,1,2,.5,.5,1,1,.5,1,1])
    PSYCHIC = dict(index=13, effect=[1,2,1,2,1,1,1,1,.5,1,1,1,1,.5,1,1,0,1])
    ICE = dict(index=14, effect=[1,1,2,1,2,1,1,1,.5,.5,.5,2,1,1,.5,2,1,1])
    DRAGON = dict(index=15, effect=[1,1,1,1,1,1,1,1,.5,1,1,1,1,1,1,2,1,0])
    DARK = dict(index=16, effect=[1,.5,1,1,1,1,1,2,1,1,1,1,1,2,1,1,.5,.5])
    FAIRY = dict(index=17, effect=[1,2,1,.5,1,1,1,1,.5,.5,1,1,1,1,1,2,2,1])
