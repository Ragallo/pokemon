FROM python:3.8-alpine

ENV PYTHONUNBUFFERED 1 
ENV PYTHONDONTWRITEBYTECODE=1

RUN mkdir /pokemon
WORKDIR /pokemon

COPY requirements.txt /pokemon/
RUN pip install -r requirements.txt

COPY . /pokemon/
